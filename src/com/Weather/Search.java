/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



/**
 *
 * @author Mkelpin
 */
public class Search {
    String CityName ;
    private static HttpURLConnection con;
    
    void GetCity() throws IOException, ParseException{
         String url = "https://www.metaweather.com/api/location/search/?query="+CityName;
        try {
            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();
            con.setRequestMethod("GET");
            StringBuilder content;
            try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
                    String line;
                    content = new StringBuilder();
                    while ((line = in.readLine()) != null) {
                        content.append(line);
                        content.append(System.lineSeparator());
                    }
                };
               
                Object obj = new JSONParser().parse(content.toString());
                JSONObject jo = (JSONObject) obj; 
                
                System.out.println(jo.get("title"));
               
        } finally {
            
            con.disconnect();
        }       
    }
  
}
